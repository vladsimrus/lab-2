#include "semaphore.h"

void semaphore::Enter()
{
    countCS.Enter();
    if ( Count++ < maxCount )
    {
        count_dbg++;
        countCS.Leave();
        return;
    }

    countCS.Leave();
    ev.Wait();

    countCS.Enter();
    count_dbg++;
    countCS.Leave();
}

void semaphore::Leave()
{
    countCS.Enter();
    if ( --Count >= maxCount )
        ev.Set();
    count_dbg--;
    countCS.Leave();
}
