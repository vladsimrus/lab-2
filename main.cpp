#include <iostream>
#include <windows.h>

#include "semaphore.h"

using namespace std;

DWORD WINAPI Func( LPVOID param );

semaphore smp( 5 );

int creating_thread = 0;

int main(int argc, char *argv[])
{
    LPVOID a;
    DWORD id;

    for ( int i = 0; i < 3; i++ ){
        a = new int( i );
        HANDLE h = CreateThread( NULL, 0, Func, a, 0, &id );
    }

    creating_thread = 1;

    while(1)
        if ( smp.return_Count_dbg() > 5 ){
            cout << "Error"; return 0 ;
        }

    return 0;
}

DWORD WINAPI Func( LPVOID param )
{
    while (!creating_thread);

    while(1)
    {
        smp.Enter();

        cout << "Enter " << *(int *)param << endl;
        int calc = 0;
        for ( int i = 0; i < 200; i++ )
        {
            calc += i + 20*i;
            Sleep(0);
        }

        cout << "Leave " << *(int *)param << endl;
        smp.Leave();
    }
}
