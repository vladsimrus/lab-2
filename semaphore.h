#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include <iostream>
#include <windows.h>

using namespace std;

class CS{
    CRITICAL_SECTION CS_;

public:
    CS(){ InitializeCriticalSection( &CS_ ); }
    void Enter(){ EnterCriticalSection( &CS_ ); }
    void Leave(){ LeaveCriticalSection( &CS_ ); }
};

class Event{
    HANDLE hEvent;
public:
    Event() { hEvent = CreateEvent( NULL, FALSE, FALSE,  L"Event" ); }
    ~Event() { CloseHandle( hEvent ); }

    void Set() { SetEvent( hEvent ); }
    void Reset() { ResetEvent( hEvent ); }

    DWORD Wait( int ms ) { return WaitForSingleObject( hEvent, ms ); }
    DWORD Wait() { return Wait( INFINITE ); }
};

class semaphore
{
    int maxCount;
    int Count;
    int count_dbg;

    CS countCS;
    Event ev;

public:
    semaphore( int count ) : maxCount( count ), Count( 0 ), count_dbg( 0 ) {}
    //~semaphore();

    void Enter();
    void Leave();

    int return_Count_dbg() { return count_dbg; }
};

#endif // SEMAPHORE_H
